---
layout: specification
title: 2022-05-15 Network Upgrade Specification
author: freetrader (Bitcoin Cash Node)
date: 2021-11-15
version: 1
---

# 2022-05-15 Network Upgrade Specification

freetrader (Bitcoin Cash Node)

Version 1, 2021-11-15

---


The following changes to the Bitcoin Cash network activate on 15 May 2022:

* [CHIP-2021-03: Bigger Script Integers](https://gitlab.com/GeneralProtocols/research/chips/-/blob/master/CHIP-2021-02-Bigger-Script-Integers.md)
  [(Discussion)](https://bitcoincashresearch.org/t/chip-2021-03-bigger-script-integers/39).
* [CHIP-2021-02: Native Introspection Opcodes](https://gitlab.com/GeneralProtocols/research/chips/-/blob/master/CHIP-2021-02-Add-Native-Introspection-Opcodes.md)
  [(Discussion)](https://bitcoincashresearch.org/t/chip-2021-02-add-native-introspection-opcodes/307).

The new consensus rules in the abovementioned CHIPs will take effect once the
median time past (MTP) [1] of the most recent 11 blocks is greater than or
equal to UNIX timestamp 1652616000.

Please refer to the corresponding CHIP resources for more information.


## References

[1] Median Time Past is described in [bitcoin.it wiki](https://en.bitcoin.it/wiki/Block_timestamp). It is guaranteed by consensus rules to be monotonically increasing.

---

## License of this document

This document is dedicated to the public domain using the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
