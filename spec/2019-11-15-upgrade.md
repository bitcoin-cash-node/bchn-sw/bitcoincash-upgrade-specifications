---
layout: specification
title: 2019-NOV-15 Network Upgrade Specification
date: 2019-10-23
category: spec
activation: 1573819200
version: 0.4
---

# 2019-NOV-15 Network Upgrade Specification

Version 0.4, 2019-10-23

---


## Summary

When the median time past [1] of the most recent 11 blocks (MTP-11) is greater than or equal to UNIX timestamp 1573819200,
Bitcoin Cash will execute an upgrade of the network consensus rules according to this specification.
Starting from the next block these consensus rules changes will take effect:

* Enable Schnorr signatures for OP_CHECKMULTISIG(VERIFY).
* Enforce minimal push and minimal number encoding rules in Script.

The following are not consensus changes, but are recommended changes for Bitcoin Cash implementations:

* Automatic replay protection for future upgrade

## Schnorr Signatures for OP_CHECKMULTISIG(VERIFY)

Use of Schnorr signatures is enabled in OP_CHECKMULTISIG(VERIFY). The dummy element is repurposed to flag
Schnorr mode when it is non-null, and the order of signatures in Schnorr mode is constrained according to
the bitfield encoded in the repurposed dummy element.

Details can be found in the [full specification: 2019-11-15-schnorrmultisig.md](2019-11-15-schnorrmultisig.md).

NOTE: The repurposing of the dummy element as a flag and bitfield supersedes the need for NULLDUMMY.

## Enforce MINIMALDATA in Script.

Enforce existing standardness checks that all executed data pushes use minimal push operators, and all numbers are encoded minimally,
together known as the "MINIMALDATA" rule. This goes into effect at the consensus layer.

Details can be found in the [full specification: 2019-11-15-minimaldata.md](2019-11-15-minimaldata.md).

## Automatic Replay Protection

The purpose of Automatic Replay Protection is to serve as a full node version-deprecation mechanism. It is intended to cause 
full validating nodes which do not upgrade, to automatically separate themselves from the main network after the next
upgrade on 15 May 2020. Nodes which implement the next upgrade will remove this automatic replay protection, and thus all regular
wallets can continue using the default ForkID with no change to follow the main upgraded chain.

When the median time past [1] of the most recent 11 blocks (MTP-11) is less than UNIX timestamp 1589544000 (May 2020 upgrade)
Bitcoin Cash full nodes MUST enforce the following rule:

 * `forkid` [2] to be equal to 0.

When the median time past [1] of the most recent 11 blocks (MTP-11) is greater than or equal to UNIX timestamp 1589544000
(May 2020 upgrade) Bitcoin Cash full nodes implementing the Nov 2019 consensus rules SHOULD enforce the following change:

 * Update `forkid` [2] to be equal to `0xFFXXXX`, where `XXXX` is some arbitrary hex value.
   ForkIDs beginning with 0xFF will be reserved for future protocol upgrades.

This particular consensus rule MUST NOT be implemented by Bitcoin Cash wallet software. Wallets that follow the upgrade
should not have to change anything.

## References

[1] Median Time Past is described in [bitcoin.it wiki](https://en.bitcoin.it/wiki/Block_timestamp).
It is guaranteed by consensus rules to be monotonically increasing.

[2] The `forkId` is defined as per the [replay protected sighash](replay-protected-sighash.md) specification.


---

## License of this document

<small>The MIT License (MIT)</small>

<small>Copyright (c) 2017-2019 bitcoincash.org</small>

<small>Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:</small>

<small>The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.</small>

<small>THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.</small>
